<?php 
 define(PRECIUNIDAD, 60);
 $cantidad = 3;
 define(IVA, 12);
 $precioSinIva = PRECIUNIDAD * $cantidad;
 $precioConIva = PRECIUNIDAD * $cantidad * ((100 + IVA)/100);

 echo 'precio con iva ' . round($precioConIva, 2)  . '<br>';
 echo 'precio sin iva ' . round($precioSinIva, 2)  . '<br>';

 ?>